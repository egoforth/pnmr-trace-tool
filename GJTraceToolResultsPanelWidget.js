define([
  "dojo/_base/declare",
  "dojo/_base/array",
  "dojo/_base/lang",
  "dojo/dom-style",
  "dojo/dom-construct",
  "dojo/dom-class",
  "dojo/on",
  "dojo/data/ItemFileWriteStore",
  "dojo/promise/all",

  "dijit/_Widget",
  "dijit/_Templated",
  "dijit/Tooltip",
  "dijit/form/Button",

  "dojox/grid/EnhancedGrid",
  "dojox/grid/enhanced/plugins/Filter",

  "ibm/tivoli/fwm/mxmap/_Base",
  "ibm/tivoli/fwm/mxmap/panels/MobileInfoPanelDialog",

  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/geometry/Point",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/symbols/SimpleFillSymbol",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/symbols/SimpleMarkerSymbol",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/symbols/SimpleLineSymbol",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/Color",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/graphic",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/tasks/QueryTask",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/tasks/query",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/layers/GraphicsLayer"
], function (
  declare, array, lang, Style, domConstruct, domClass, on, ItemFileWriteStore, all,
  _Widget, _Templated, Tooltip, Button,
  EnhancedGrid, Filter,
  _Base, Dialog,
  Point, SimpleFillSymbol, SimpleMarkerSymbol, SimpleLineSymbol, Color, Graphic, QueryTask, Query, GraphicsLayer
) {
  declare( "GJTraceToolResultsPanelWidget", [dijit._Widget, dijit._Templated, ibm.tivoli.fwm.mxmap._Base], {
    templateString : dojo.cache( "ibm.tivoli.fwm.mxmap.toolbar.ext", "GJTraceToolResultsPanel.html" ),
    infoPanel : null,
    map : null,
    _title : null,
    store : null,
    defaultSymbolColor : null,
    defaultPageSize : 10,
    resultsGraphicLayer: null,
    resizePaginatorEvent : null,
    totalFeaturesAdded : null,
    countFeaturesAdded : null,
    customButtonArray : null,
    dialogPosition : null,
    layersRequested : null,
    featuresAdded : null,
    jsonGraphicsArray : null,
    currentPage : null,
    returnAttribute : null,
    tool : null,
    resultSpatialReference: null,

    constructor : function ( params ) {
        dojo.mixin( this, params );
        this.customButtonArray = [];
        this.resizePaginatorEvent = [];
        this.featuresAdded = [];
        this.returnAttribute = this.map.mapConf.returnAttribute;
        this.defaultSymbolColor = new Color([255, 0, 0, 0.5]);
        this._title = "Results";
        dojohelper.loadfile( dojo.config.dojoDirectory + "/dojox/grid/enhanced/resources/tundra/EnhancedGrid.css", "css" );
        dojohelper.loadfile( dojo.config.dojoDirectory + "/dojox/grid/enhanced/resources/EnhancedGrid_rtl.css", "css" );
    },

    postCreate : function () {},

    close : function () {
        this.resultsGraphicLayer.clear();
        this.clearGrid();
        this.infoPanel.close();
    },

    destroy : function () {
        this.infoPanel.destroyDialog();
        this.inherited( arguments );
    },

    createDialog : function () {
        try {
            this.jsonGraphicsArray = null;
            this.createButtons();
            this.featuresAdded = [];
            this.currentPage = null;
            var map = this.map;
            this.infoPanel = ibm.tivoli.fwm.mxmap.panels.MobileInfoPanelDialog( {
                map : this.map,
                title : this._title,
                nonModal : true,
                customButtons : this.customButtonArray,
                isResizable : true,
                btnDivClass : "resultsToolBtnsDraw",
                minWidth : 500,
                position : this.dialogPosition,
                tool : this.tool
            } );
            this.infoPanel.setContent( this.domNode );
            // set up layout
            var layout = [
              { name: 'Object ID', field: 'objectId', width: '20%' },
              { name: 'MX Asset #', field: 'assetNum', width: '30%' },
              { name: 'Map Layer', field: 'layer', width: '50%' }
            ];
            this.store = new ItemFileWriteStore( {
              data : { identifier : 'key', items : [] }
            } );
            var existedGrid = dijit.byId( "grid" + this.infoPanel.dialogId );
            if ( existedGrid ) {
                existedGrid.destroyRecursive( false );
            }
            // create a new grid:
            this.grid = new EnhancedGrid( {
                id : 'grid' + this.infoPanel.dialogId,
                store : this.store,
                structure : layout,
                style : 'width: 100%; height: 175px;',
                rowSelector : '10px',
                selectionMode : 'multiple',
                keepSelection : true,
                query : {},
                autoHeight : false,
                plugins : {
                    filter : {
                        closeFilterbarButton : false,
                        ruleCount : 5,
                        // Set the name of the items
                        itemsName : "features"
                    }
                }
            }, domConstruct.create( "div" ) );

            on( this.grid, "rowClick", lang.hitch( this, function ( event ) {
                this.highlightFeature( event );
            } ), this );

        } catch ( error ) {
            console.error( "Failed on Results widget create method.", error);
        }

    },

    clearGrid : function () {
        this.store = new ItemFileWriteStore( {
            data : { identifier : 'key', items : [] }
        } );
        this.grid.setStore( this.store );
        this.grid.selection.clear();
        this.resultsGraphicLayer.clear();
        this.infoPanel.resizeGrid();
        this.featuresAdded = [];
    },

    updateFieldValue : function () {
        var maximoReturnAttribute = this.map.mapConf.returnAttribute;
        var selectedFeatures = this.grid.selection.getSelected();

        if ( selectedFeatures.length > 1 ) {
            console.log( "only 1 feature can be selected" );
        } else {
            maximoObjectFeatureAttributes = selectedFeatures[ 0 ].feature[ 0 ].attributes;
            if ( maximoReturnAttribute )
                this.map.getMaximo().updateFieldValue( maximoReturnAttribute, maximoObjectFeatureAttributes,
                        lang.hitch( this, function ( data ) {
                            console.log( data );
                        } ) );
        }
    },

    updateApplicationList : function () {
        var maximoObjectIdValues = new Array();
        var maximoObjectIdName = null;
        var currentMbo = this.map.mapConf.currentMbo;
        if ( !currentMbo ) {
            extendsMboName = this.map.mapConf.parentApplication;
        } else {
            extendsMboName = currentMbo.mxdata.extendsMboName;
        }
        this.store.fetch( {
            query : { type : extendsMboName },
            // Fetches for records that has the same type of the current application.
            onComplete : lang.hitch( this, function ( items ) {
                array.forEach( items, function ( item ) {
                    maximoObjectIdValues.push( item.maximoObjectIdValue[ 0 ] );
                    if ( maximoObjectIdName == null )
                        maximoObjectIdName = item.maximoObjectIdName[ 0 ];
                } );
            } )
        } );
        /*
         * Calls the server side method that will update the list tab with the selected records only if there are
         * records to be updated.
         */
        if ( maximoObjectIdValues.length > 0 )
            this.map.getMaximo().updateApplicationList( maximoObjectIdName, maximoObjectIdValues,
                    lang.hitch( this, function ( data ) {
                    } ) );
    },

    updateApplicationListFromListView : function () {

        var maximoReturnAttribute = this.map.mapConf.returnAttribute;
        var selectedFeatures = this.grid.selection.getSelected();
        var attributes = new Array();

        for ( var i = 0; i < selectedFeatures.length; i++ ) {
            attributes.push( selectedFeatures[ i ].feature[ 0 ].attributes );
        }

        if ( maximoReturnAttribute )
            this.map.getMaximo().updateApplicationListFromListView( maximoReturnAttribute, attributes,
                    lang.hitch( this, function ( data ) {
                    } ) );
    },

    mergeMapResultsList : function () {
        this.loadFeaturesFromListTabLinkedMbosToGrid();
    },

    updateMapResultsList : function () {
        this.clearGrid();
        this.loadFeaturesFromListTabLinkedMbosToGrid();
    },

    loadFeaturesFromListTabLinkedMbosToGrid : function () {
        this.layersRequested = [];
        this.map.getMaximo().getArcGISQueryAttributesFromListTabLinkedMbos( lang.hitch( this, function ( data ) {
            if ( data.arcGISQueryAttributes ) {
                var aDeferreds = [];
                array.forEach( data.arcGISQueryAttributes, function ( arcGISQueryAttribute, i ) {
                    var queryTask = new QueryTask( arcGISQueryAttribute.url + "/" + arcGISQueryAttribute.layerId );
                    var query = new Query();
                    query.outSpatialReference = this.map.map.spatialReference;
                    query.returnGeometry = true;
                    query.spatialRelationship = esri.tasks.Query.SPATIAL_REL_CONTAINS;
                    query.outFields = ["*"];
                    query.where = arcGISQueryAttribute.whereClause;

                    var defferedTask = queryTask.execute( query );
                    aDeferreds.push( defferedTask );
                    this.layersRequested[ i ] = {
                        "layerName" : arcGISQueryAttribute.layerName,
                        "layerGroup" : arcGISQueryAttribute.layerGroup,
                        "layerId" : arcGISQueryAttribute.layerId
                    }
                }, this );
                promises = all( aDeferreds );
                promises.then( lang.hitch( this, this.handleQueryResults ), lang.hitch( this, function ( err ) {
                    console.error( 'query error', err );
                    this.map.getMaximo().showMessage( "mapQueryAttributesTool", "errorRunningQuery", [err] );
                } ) );
            }
        } ) );
    },

    handleQueryResults : function ( results ) {
        var features = [];
        array.forEach( results, function ( fset, i ) {
            if ( fset.features ) {
                array.forEach( fset.features, function ( feat, j ) {
                    var layerInfo = this.layersRequested[ i ];
                    feat.attributes.layerName = layerInfo.layerName;
                    feat.attributes.layerGroup = layerInfo.layerGroup;
                    feat.attributes.layerId = layerInfo.layerId;
                    features.push( feat );
                }, this );
            }
        }, this );
        this.addFeaturesToResults( features );
    },

    createButtons : function () {
        // Clear button.
        var clearBtn = this.createButtonParams( dojo.hitch( this, function () {
            this.clearGrid();
        } ), "", false, "toolBtnClear", {
            label : ibm.tivoli.fwm.i18n.getMaxMsg( "map", "selectionToolClear" ),
            showDelay : this.map.showDelayTimeTooltip
        } );
        this.customButtonArray.push( clearBtn );

        if ( !this.map.mapConf.returnAttribute == "" && !this.map.mapConf.calledByListView ) {
            // Update Field Button
            var updateFieldValue = this.createButtonParams( dojo.hitch( this, function () {
                this.updateFieldValue();
            } ), "", false, "toolBtnUpdateApplicationList", {
                label : ibm.tivoli.fwm.i18n.getMaxMsg( "plussmap", "toolbar_update_mx_list_title" ),
                showDelay : this.map.showDelayTimeTooltip
            } );
            //this.customButtonArray.push( updateFieldValue );

            } else if ( this.map.mapConf.calledByListView ) {
                // Update Application Button.
                var updateApplicationBtn = this.createButtonParams( dojo.hitch( this, function () {
                    this.updateApplicationListFromListView();
                } ), "", false, "toolBtnUpdateApplicationList", {
                    label : ibm.tivoli.fwm.i18n.getMaxMsg( "plussmap", "toolbar_update_mx_list_title" ),
                    showDelay : this.map.showDelayTimeTooltip
                } );
                //this.customButtonArray.push( updateApplicationBtn );

            } else {
                // Update Application Button.

                var updateApplicationBtn = this.createButtonParams( dojo.hitch( this, function () {
                    this.updateApplicationList();
                } ), "", false, "toolBtnUpdateApplicationList", {
                    label : ibm.tivoli.fwm.i18n.getMaxMsg( "plussmap", "toolbar_update_mx_list_title" ),
                    showDelay : this.map.showDelayTimeTooltip
                } );
                //this.customButtonArray.push( updateApplicationBtn );

                // Update Map Button.

                var updateMapBtn = this.createButtonParams( dojo.hitch( this, function () {
                    this.updateMapResultsList();
                } ), "", false, "toolBtnUpdateMap", {
                    label : ibm.tivoli.fwm.i18n.getMaxMsg( "plussmap", "toolbar_update_gis_list_title" ),
                    showDelay : this.map.showDelayTimeTooltip
                } );
                //this.customButtonArray.push( updateMapBtn );

                // Merge List button.

                var mergeBtn = this.createButtonParams( dojo.hitch( this, function () {
                    this.mergeMapResultsList();
                } ), "", false, "toolBtnMerge", {
                    label : ibm.tivoli.fwm.i18n.getMaxMsg( "plussmap", "toolbar_merge_mx_gis_list_title" ),
                    showDelay : this.map.showDelayTimeTooltip
                } );

                //this.customButtonArray.push( mergeBtn );
            }

        },

        createButtonParams : function ( action, label, showLabel, iconClass, tooltip ) {
            return btnParams = {
                label : label,
                showLabel : showLabel,
                iconClass : iconClass,
                onClick : dojo.hitch( this, function () {
                    action();
                } ),
                toolTip : tooltip
            };
        },

        resizeDialogOnPaginationClick : function () {
            array.forEach( this.resizePaginatorEvent, function ( event, i ) {
                event.remove();
            }, this );

            var paginatorElements = dojo.query( ".dojoxGridPaginator" );
            array.forEach( paginatorElements, function ( element, i ) {
                var event = on( element, "click", lang.hitch( this, function ( e ) {
                    if ( this.infoPanel.resized ) {
                        this.infoPanel.resizeGrid( false );
                    } else {
                        this.infoPanel.resizeGrid( true );
                    }

                } ) );
                this.resizePaginatorEvent.push( event );
            }, this );
        },

        showDialog : function () {
            if ( this.infoPanel ) {
                this.infoPanel.show();
                dojo.place(this.grid.domNode, this.infoPanel.center, 'first');
                this.grid.startup();
                //this.infoPanel.resizeGrid();
            }
        },

        showDialogRelative : function ( dialogReference ) {
            if ( this.infoPanel ) {
                var p = this.map.map.position;
                var dx = this.map.map.width + p.x;
                var dy = dialogReference.domNode.offsetHeight + p.y + 2;
                this.infoPanel.showPosition( dx, dy );
                dojo.place( this.grid.domNode, this.infoPanel.center, 'first' );
                this.grid.startup();
                this.resizeDialogOnPaginationClick();
                this.infoPanel.resizeGrid();
                this.infoPanel.resizeGrid();
            }
        },

        hideDialog : function () {
            if ( this.infoPanel ) {
                this.infoPanel.close();
            }
        },

        highlightFeature : function ( event ) {
            if ( event ) {
                var gridItem = this.grid.getItem( event.rowIndex );
                var feature = gridItem.feature[ 0 ];
                if ( feature.geometry ) {
                    var extent = null;
                    var point = feature.geometry;
                    var maxZoom = this.map.map.getMaxZoom();
                    var zoomLevel = 12;
                    if ( zoomLevel > maxZoom ) zoomLevel = maxZoom;
                    this.map.map.centerAndZoom( point, zoomLevel );
                } else {
                    this.map.showWarningMessage( "map", "selectiontoolnoshape", null );
                }
            }

            var symbol = null;
            var selectedItems = this.grid.selection.getSelected();
            this.resultsGraphicLayer.clear();
            array.forEach( selectedItems, function ( selectedItem ) {
                if (selectedItem.feature.length > 0) {
                var selectedFeature = selectedItem.feature[ 0 ];
                if ( selectedFeature.geometry ) {
                    var objectId = selectedFeature.attributes.objectid || selectedFeature.attributes.OBJECTID;
                    if ( objectId ) {
                        symbol = new SimpleMarkerSymbol();
                        symbol.setColor( this.defaultSymbolColor );
                        symbol.setOutline( new SimpleLineSymbol( SimpleLineSymbol.STYLE_NULL, this.defaultSymbolColor, 1 ) );
                        symbol.setStyle( SimpleMarkerSymbol.STYLE_CIRCLE );
                        symbol.setSize(24);
                        hasFeatures = true;
                        var graphic = new Graphic( new Point(selectedFeature.geometry.x, selectedFeature.geometry.y, this.resultSpatialReference), symbol );
                        if ( graphic.geometry && graphic.symbol ) {
                            this.resultsGraphicLayer.add( graphic );
                        }
                    }
                }
                }
            }, this );
        },

        updateGrid : function ( ) {
            if ( this.countFeaturesAdded == this.totalFeaturesAdded ) {
                this.grid.store.save();
                this.grid.setFilter( this.grid.getFilter() );
                this.grid.render();
            }
        },

        hideButtonsForOpenMap : function () {
            var buttons = this.customButtonArray;
            var label = ibm.tivoli.fwm.i18n.getMaxMsg( "plussmap", "toolbar_update_gis_list_title" );
            for ( var i = 0; i < buttons.length; i++ ) {
                if ( buttons[ i ].label == label ) {
                    domClass.add( buttons[ i ], "hideLayerFeatures" );
                }
            }
        },

        loadGraphics : function () {
            this.resultsGraphicLayer.clear();

            var resultsGraphics = this.jsonGraphicsArray;
            if ( resultsGraphics ) {
                var resultsLength = resultsGraphics.length;
                for ( var j = 0; j < resultsLength; j++ ) {
                    var newGraphic = new Graphic( resultsGraphics[ j ] );
                    this.resultsGraphicLayer.add( newGraphic );

                }
            }
        },

        addFeaturesToResults : function ( arrayFeatures, highlightFeatures ) {
            this.map.showLoadingImg();
            this.clearGrid();
            this.totalFeaturesAdded = arrayFeatures.length;
            this.countFeaturesAdded = 0;
            array.forEach( arrayFeatures, lang.hitch(this, function (feature, i ) {
                newItem = {
                    key : feature.attributes.OBJECTID + "GJTraceTool1",
                    objectId : parseInt( feature.attributes.OBJECTID ),
                    type : 'Unlinked',
                    assetNum : feature.attributes.MXASSETNUM,
                    maximoObjectIdValue : null,
                    maximoObjectIdName : null,
                    layer : "Trace Tool",
                    feature : feature
                };
                this.grid.store.newItem( newItem );
                this.countFeaturesAdded++
                this.featuresAdded.push( feature );
                this.updateGrid( );
            }));

            this.map.hideLoadingImg();
            var me = this;
            setTimeout( function () {
                me.infoPanel.resizeGrid();
                if ( me.currentPage ) {
                    me.grid.pagination.plugin.currentPage( me.currentPage );
                    me.infoPanel.resizeGrid();
                    me.infoPanel.resizeGrid();
                }
            }, 300 );
        }
    } );
    return "GJTraceToolResultsPanelWidget";
});
