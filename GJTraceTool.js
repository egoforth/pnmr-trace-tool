dojo.require("ibm.tivoli.fwm.mxmap.toolbar.ext._ToolSpatialTemplate");
dojo.require("ibm.tivoli.fwm.mxmap.toolbar.ext.GJTraceToolPanelWidget");
dojo.provide("ibm.tivoli.fwm.mxmap.toolbar.ext.GJTraceTool");
dojo.declare("ibm.tivoli.fwm.mxmap.toolbar.ext.GJTraceTool", ibm.tivoli.fwm.mxmap.toolbar.ext._ToolSpatialTemplate, {

  // The label that will be shown as a tooltip for the tool
  label: "Trace Tools",
  toolName: "GJTraceTool",

  // CSS class that sets the button's icon
  iconClass: "basicMapToolbarBtn GJTraceToolToolbarBtn",

  // The reference to the Map object (set by the constructor)
  map: null,
  modifiesMap: false,
  panelWidget: null,

  // Tool initialization. The params argument, by default, contains only the Map reference
  constructor: function(params) {
    dojo.mixin(this, params);
    // Add our own stylesheet in
    dojo.query("head")[0].innerHTML += '<link rel="stylesheet" type="text/css" href="/maximo/webclient/javascript/ibm/tivoli/fwm/mxmap/toolbar/ext/GJTraceTool.css">';
  },

  // This method is called when the button is clicked
  execute: function() {
    if(!this.panelWidget) {
      this.map.showLoadingImg();
      this.panelWidget = new GJTraceToolPanelWidget({ map: this.map, dialogPosition: this.dialogPosition, tool: this });
      this.panelWidget.createDialog();
      this.map.hideLoadingImg();
    }
    this.panelWidget.showPanel();
  },

  disable: function() {
    if (this.panelWidget) { this.panelWidget.closePanel(); }
  },

});
