require([
  "dojo/_base/declare",
  "dojo/promise/all",
  "dojo/Deferred",
  "dojo/DeferredList",
  "dojo/store/Memory",
  "dojo/dom-construct",
  "dojo/on",
  "dojo/_base/array",
  "dojo/dom-style",
  "dojo/dom-class",
  "dojo/_base/lang",
  "dojo/query",

  "dojox/widget/Standby",

  "dijit/_Widget",
  "dijit/_Templated",
  "dijit/registry",
  "dijit/Dialog",
  "dijit/form/ComboBox",
  "dijit/form/NumberSpinner",
  "dijit/form/FilteringSelect",
  "dijit/layout/TabContainer",
  "dijit/layout/ContentPane",
  "dijit/form/Select",
  "dijit/form/Button",
  "dijit/Tooltip",

  "ibm/tivoli/fwm/mxmap/_Base",

  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/toolbars/draw",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/graphic",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/InfoTemplate",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/layers/GraphicsLayer",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/layers/FeatureLayer",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/request",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/geometry/Polygon",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/geometry/Point",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/geometry/Extent",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/SpatialReference",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/symbols/SimpleFillSymbol",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/symbols/SimpleMarkerSymbol",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/symbols/SimpleLineSymbol",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/tasks/query",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/tasks/QueryTask",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/tasks/Geoprocessor",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/tasks/ProjectParameters",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/tasks/GeometryService",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/Color",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!esri/graphicsUtils",
  "ibm/tivoli/fwm/mxmap/LoadEsriPlugin!dojo/i18n!esri/nls/jsapi"
 ], function (
  declare, all, Deferred, DeferredList, Memory, domConstruct, on, array, Style, domClass, lang, query,
  Standby,
  _Widget, _Templated, registry, Dialog, ComboBox, NumberSpinner, FilteringSelect, TabContainer, ContentPane, Select, Button, Tooltip,
  _Base,
  Draw, Graphic, InfoTemplate, GraphicsLayer, FeatureLayer, esriRequest, Polygon, Point, Extent, SpatialReference, SimpleFillSymbol, SimpleMarkerSymbol, SimpleLineSymbol, esriQuery, QueryTask, Geoprocessor, ProjectParameters, GeometryService, Color, graphicsUtils, bundle
 ) {
  declare( "GJTraceToolPanelWidget", [dijit._Widget, dijit._Templated, ibm.tivoli.fwm.mxmap._Base], {
    templateString : dojo.cache( "ibm.tivoli.fwm.mxmap.toolbar.ext", "GJTraceToolPanel.html" ),
    map : null,
    infoPanel : null,
    _title : null,
    _graphicsLayer: null,
    _runningOverhead: false,
    _runningUnderground: false,
    _selectTool: null,
    _feedersGeo: {},
    _feederNames: [],
    _tracetype: null,
    _resultsToolPanel: null,
    _resultsPanelLayer: null,
    dialogPosition: null,
    tool: null,
    widgetStandby: null,
    userSite: null,
    overheadGPURL: null,
    overheadRESURL: null,
    overheadDataModel: null,
    undergroundGPURL: null,
    undergroundRESURL: null,
    undergroundDataModel: null,
    traceResultFeatures: null,

    constructor : function ( params ) {
      dojo.mixin( this, params );
      this._title = "Trace Tools";
      this.userSite = this.map.mapConf.SPATIAL.mapInfo.siteID;
      var CONFIG = dojo.cache("ibm.tivoli.fwm.mxmap.toolbar.ext", "GJTraceTool_Config_"+this.userSite+".json");
      CONFIG = JSON.parse(CONFIG);
      
      if (!CONFIG) { this._showError("ERROR:  Config File Invalid or Missing."); }
      else {
        dojo.mixin( this, CONFIG );
        if (this.userSite == "PNM") { // NEW MEXICO
          this.overheadGPURL = "http://albarcfmwebq1/arcfm/rest/services/DEV/NewMexicoOverheadTrace/GPServer/NewMexicoOverheadTrace";
          this.overheadRESURL = "http://albarcfmwebq1/arcfm/rest/directories/arcgisjobs/dev/newmexicooverheadtrace_gpserver";
          this.overheadDataModel = ['PNM.SUPPORTSTRUCTURE'];
          this.undergroundGPURL = "http://albarcfmwebq1/arcfm/rest/services/DEV/NewMexicoUndergroundTrace/GPServer/NewMexicoUndergroundTrace";
          this.undergroundRESURL = "http://albarcfmwebq1/arcfm/rest/directories/arcgisjobs/dev/newmexicoundergroundtrace_gpserver";
          this.undergroundDataModel = ['PNM.UNDERGROUNDSTRUCTURE', 'PNM.SWITCHINGFACILITY', 'PNM.SURFACESTRUCTURE', 'PNM.VAULT', 'PNM.CONTACTOR', 'PNM.PFCORRECTINGEQUIPMENT'];
        } else if (this.userSite == "TNMP") { // TEXAS
          this.overheadGPURL = "http://albarcfmwebq1/arcfm/rest/services/DEV/TexasOverheadTrace/GPServer/TexasOverheadTrace";
          this.overheadRESURL = "http://albarcfmwebq1/arcfm/rest/directories/arcgisjobs/dev/texasoverheadtrace_gpserver";
          this.overheadDataModel = ['SupportStructure_FeaturesToJ2'];
          this.undergroundGPURL = "http://albarcfmwebq1/arcfm/rest/services/DEV/TexasUndergroundTrace/GPServer/1.%20TexasUndergroundTrace";
          this.undergroundRESURL = "http://albarcfmwebq1/arcfm/rest/directories/arcgisjobs/dev/texasundergroundtrace_gpserver";
          this.undergroundDataModel = ['undergroundStructures', 'undergroundSwitches', 'undergroundSwitchFacility', 'undergroundSurfaceStructure', 'undergroundVoltageRegulator', 'undergroundFuse', 'undergroundPFCorrectingEquipment'];
        } else { // unknown user site
          this._showError("'" + this.userSite + "' is not a known user insert site -- trace tool cannot continue.");
        }
      }
    },

    createDialog : function () {
      this.infoPanel = new Dialog({
        'id': "GJTraceToolDialog",
        'title': this._title,
        'content': this.domNode,
        'style': "width: 375px; height: 325px;",
        'class': "GJTraceTool nonModal"
      });

      // Programatically create a TabContainer and its Content Panes
      var tc = new TabContainer({ style: "position:absolute;top:23px;left:2px;bottom:2px;right:2px;", id:'tabContainer' });
      var cp1 = new ContentPane({ title: "Perform Trace", content: "", id:"performTracePane" });
      var cp2 = new ContentPane({ title: "Results", content: "", id:"traceResultsPane" });
      cp2.set("disabled", true);

      tc.addChild(cp1);
      tc.addChild(cp2);
      tc.startup();
      this.infoPanel.set("content", tc.domNode);

      // Standby Widget
      var tracerunHTML = "<p style='color:#FFF; font-size: 14px; text-align: center; cursor: wait;'>Running Trace</p>"
                      + "<p style='color:#FFF; font-size: 12px; text-align: center; cursor: wait;'>This process may take several minutes to complete.</p>"
                      + "<div class='GJTraceToolLoading'></div>";
      var addfeaturesHTML = "<p style='color:#FFF; font-size: 14px; text-align: center; cursor: wait;'>Adding Features</p>"
                      + "<p style='color:#FFF; font-size: 12px; text-align: center; cursor: wait;'>This process may take several minutes to complete.</p>"
                      + "<div class='GJTraceToolLoading'></div>";
      var loadingSubstationsHTML = "<p style='color:#FFF; font-size: 14px; text-align: center; cursor: wait;'>Loading Substations</p>"
                      + "<p style='color:#FFF; font-size: 12px; text-align: center; cursor: wait;'>Please wait.</p>"
                      + "<div class='GJTraceToolLoading'></div>";
      var loadingFeedersHTML = "<p style='color:#FFF; font-size: 14px; text-align: center; cursor: wait;'>Loading Feeders</p>"
                      + "<p style='color:#FFF; font-size: 12px; text-align: center; cursor: wait;'>Please wait.</p>"
                      + "<div class='GJTraceToolLoading'></div>";

      this.widgetStandby = new Standby({target: "GJTraceToolDialog", text: tracerunHTML, centerIndicator: "text", color:"#333333"});
      document.body.appendChild(this.widgetStandby.domNode);
      this.widgetStandby.startup();

      this.addStandby = new Standby({target: "GJTraceToolDialog", text: addfeaturesHTML, centerIndicator: "text", color:"#333333"});
      document.body.appendChild(this.addStandby.domNode);
      this.addStandby.startup();

      this.loadingSubstationsStandby = new Standby({target:"GJTraceToolDialog", text: loadingSubstationsHTML, centerIndicator: "text", color:"#333333"});
      document.body.appendChild(this.loadingSubstationsStandby.domNode);
      this.loadingSubstationsStandby.startup();

      this.loadingFeedersStandby = new Standby({target:"GJTraceToolDialog", text: loadingFeedersHTML, centerIndicator: "text", color:"#333333"});
      document.body.appendChild(this.loadingFeedersStandby.domNode);
      this.loadingFeedersStandby.startup();

      // Perform Trace tab -- programatically create form elements and add them
      var substationLabel = dojo.create("label", {for:"substation", innerHTML:"Substation: "});
      var substationInput = dojo.create("input", {type:"text", id:"substationInput"});
      var feederidLabel = dojo.create("label", {for:"feederid", innerHTML:"Feeder ID: "});
      var feederidInput = dojo.create("input", {type:"text", id:"feederidInput"});
      var bufferLabel = dojo.create("label", {for:"buffer", innerHTML:"Buffer: "});
      var bufferInput = dojo.create("input", {type:"text", id:"bufferInput"});
      var bufferText = dojo.create("span", {style:"float:left; margin-left: 5px; line-height: 42px;", innerHTML:"Ft."});
      var tracetypeDiv = dojo.create("div", {id: "tracetypeDiv", style:"clear: both; text-align: center;"});
      var tracetypeInput1 = dojo.create("input", {type:"radio", name:"tracetype", value:"overhead", id:"tracetypeInput1", checked:'checked'});
      var tracetypeInput1Label = dojo.create("label", {for:"tracetypeInput1", innerHTML:"Above Ground"});
      var tracetypeInput2 = dojo.create("input", {type:"radio", name:"tracetype", value:"underground", id:"tracetypeInput2"});
      var tracetypeInput2Label = dojo.create("label", {for:"tracetypeInput2", innerHTML:"Underground"});
      var tracetypeInput3 = dojo.create("input", {type:"radio", name:"tracetype", value:"both", id:"tracetypeInput3"});
      var tracetypeInput3Label = dojo.create("label", {for:"tracetypeInput3", innerHTML:"Both"});
      var buttonsDiv = dojo.create("div", {id:"buttonsDiv", style:"clear: both; text-align: right; margin: 15px 0px;"});
      var begintraceButton = dojo.create("button", {type:"button", id:"begintrace"});

      dojo.place(substationLabel, cp1.containerNode);
      dojo.place(substationInput, cp1.containerNode);
      dojo.place(feederidLabel, cp1.containerNode);
      dojo.place(feederidInput, cp1.containerNode);
      dojo.place(bufferLabel, cp1.containerNode);
      dojo.place(bufferInput, cp1.containerNode);
      dojo.place(bufferText, cp1.containerNode);
      dojo.place(tracetypeDiv, cp1.containerNode);
      dojo.place(tracetypeInput1, tracetypeDiv);
      dojo.place(tracetypeInput1Label, tracetypeDiv);
      dojo.place(tracetypeInput2, tracetypeDiv);
      dojo.place(tracetypeInput2Label, tracetypeDiv);
      dojo.place(tracetypeInput3, tracetypeDiv);
      dojo.place(tracetypeInput3Label, tracetypeDiv);
      dojo.place(buttonsDiv, cp1.containerNode);
      dojo.place(begintraceButton, buttonsDiv);

      var substationFS = new FilteringSelect({id: "substation", name: "substation", value: "", required:true}, "substationInput").startup();
      var feederidFS = new FilteringSelect({id: "feederid", name: "feederid", value: "", required:true, disabled:true}, "feederidInput").startup();
      var bufferNS = new NumberSpinner({id: "buffer", name: "buffer", value: 40, smallDelta:5, constraints:{min:0, max:500, places:0}}, "bufferInput").startup();
      var begintraceBtn = new Button({label:"Begin Trace"}, "begintrace").startup();

      // Results tab -- programatically create form elements and add them.
      var resultsText = dojo.create("div", {id: "resultsText", style:"margin: 15px 0px; text-align: center;", innerHTML:"Features Selected: "});
      var resultsButtons1 = dojo.create("div", {id: "resultsButtons1", style:"margin: 15px 0px; text-align: center;"});
      var resultsButtons2 = dojo.create("div", {id: "resultsButtons2", style:"margin: 15px 0px; text-align: center;"});
      var resultsButtons3 = dojo.create("div", {id: "resultsButtons3", style:"margin: 15px 0px; text-align: center;"});
      var resultsButtons4 = dojo.create("div", {id: "resultsButtons4", style:"margin: 15px 0px; text-align: center;"});
      var resultsLink = dojo.create("a", {id:"resultsLink", href:"#", innerHTML:"----"});
      var resultsOpenRT = dojo.create("button", {type:"button", id:"resultsOpenRT"});
      var resultsZoomResults = dojo.create("button", {type:"button", id:"resultsZoomResults"});
      var resultsZoomSource = dojo.create("button", {type:"button", id:"resultsZoomSource"});
      var resultsClear = dojo.create("button", {type:"button", id:"resultsClear"});
      var resultsAdd = dojo.create("button", {type:"button", id:"resultsAdd"});
      var resultsRemove = dojo.create("button", {type:"button", id:"resultsRemove"});
      var resultsARLabel = dojo.create("div", {id: "resultsARLabel", innerHTML: "", style:"text-align: center;"});

      dojo.place(resultsText, cp2.containerNode);
      dojo.place(resultsButtons1, cp2.containerNode);
      dojo.place(resultsButtons2, cp2.containerNode);
      dojo.place(resultsButtons3, cp2.containerNode);
      dojo.place(resultsButtons4, cp2.containerNode);
      dojo.place(resultsLink, resultsText);
      dojo.place(resultsOpenRT, resultsButtons1);
      dojo.place(resultsZoomResults, resultsButtons2);
      dojo.place(resultsZoomSource, resultsButtons2);
      dojo.place(resultsClear, resultsButtons3);
      dojo.place(resultsARLabel, resultsButtons4);
      dojo.place(resultsAdd, resultsButtons4);
      dojo.place(resultsRemove, resultsButtons4);

      var resultsOpenRTBtn = new Button({label:"Open Results Tool"}, "resultsOpenRT").startup();
      var resultsZoomResultsBtn = new Button({label:"Zoom to Results"}, "resultsZoomResults").startup();
      var resultsZoomSourceBtn = new Button({label:"Zoom to Source"}, "resultsZoomSource").startup();
      var resultsClearBtn = new Button({label:"Clear Results From Map"}, "resultsClear").startup();
      var resultsAddBtn = new Button({label:"Add to Results", iconClass:"dijitEditorIcon dijitEditorIconCopy", class:"smallerbutton"}, "resultsAdd").startup();
      var resultsRemoveBtn = new Button({label:"Remove from Results", iconClass:"dijitEditorIcon dijitEditorIconDelete", class:"smallerbutton"}, "resultsRemove").startup();

      // Populate Substations
      this._populateSubstations();

      // Update Feeder List when substation changes
      on(registry.byId('substation'), "change", lang.hitch(this, function() {
        var emptyStore = new Memory({data:[]});
        var feederID = registry.byId('feederid');
        feederID.setAttribute("store", emptyStore);
        feederID.setAttribute("disabled", true);
        feederID.setAttribute("value", "");
        this._populateFeeders(registry.byId('substation').value);
      }));


      // Begin Trace button
      on(registry.byId('begintrace'), 'click', lang.hitch(this, function() {

        // form validation
        if (!registry.byId('substation').isValid() || !registry.byId('feederid').isValid() || !registry.byId('buffer').isValid()) {
          this._showError("Trace cannot start, one or more entries is invalid.");
          return;
        }

        this._tracetype = query('input[type=radio][name=tracetype]:checked')[0].value;
        this.widgetStandby.show();

        this._traceResultFeatures = null; // clear this out before the trace
        var tc = new Deferred(); // using this to time the trace complete after all traces have finished

        if (this._tracetype == "overhead" || this._tracetype == "both") {
          this._runningOverhead = true;
          var gp = new Geoprocessor(this.overheadGPURL);
          if (this.userSite == "PNM") {
            var params = {"Feeder_ID": ""+registry.byId('feederid').value,
                          "Buffer_Distance": registry.byId('buffer').value+" Feet"
                         };
          } else if (this.userSite == "TNMP") {
            var params = {"Expression_for_Starting_Flag__Feeder_Id_": "FEEDERID = '"+registry.byId('feederid').value+"'",
                          "Distance__value_or_field_": { "distance": registry.byId('buffer').value, "units": "esriFeet" }
                         };
          }
          gp.submitJob(params);
          gp.on('job-complete', lang.hitch(this, function(response) {

            if (response.jobInfo.jobStatus == "esriJobFailed") {
              this._showError("Overhead trace failed.");
              console.log("Overhead trace script returned fail.", response);
              this.widgetStandby.hide();
              return;
            }

            // Sleep for 2 seconds to give files ample time to be written
            var sleep = new Deferred();
            setTimeout(function() { sleep.resolve(); }, 2000);
            sleep.then(lang.hitch(this, function() {
              var oresultsFinished = 0;
              var oresultsDef = new Deferred();

              array.forEach(this.overheadDataModel, lang.hitch(this, function(thisDataModel) {
                new esriRequest({"url": this.overheadRESURL + "/" + response.jobInfo.jobId + "/scratch/" + thisDataModel + ".json"})
                    .then(lang.hitch(this,function(response,io) {
                      var projDef = new Deferred();

                      if (response.spatialReference != this.map.map.spatialReference) { // need to project geometry
 
                        var params = new ProjectParameters();
                        params.geometries = [];
                        params.outSR = this.map.map.spatialReference;

                        array.forEach(response.features, function(f) {
                          params.geometries.push(new Point(f.geometry.x, f.geometry.y, new SpatialReference(response.spatialReference)));
                        });

                        var gsvc = new GeometryService(this.Geometry_Server);
                        gsvc.project(params, lang.hitch(this, function(p) {
                          array.forEach(p, lang.hitch(this, function(f, i) { response.features[i].geometry = f; }));
                          projDef.resolve();
                        }));
                        
                      } else { projDef.resolve(); }

                      projDef.then(lang.hitch(this, function() {
                        if (!this._traceResultFeatures) this._traceResultFeatures = response.features;
                        else this._traceResultFeatures = this._traceResultFeatures.concat(response.features);
                        if (++oresultsFinished == this.overheadDataModel.length) oresultsDef.resolve();
                      }));
                    }),lang.hitch(this,function(error, io) {
                      this._showError("Error retrieving overhead trace results.");
                    }));
              }));

              oresultsDef.then(lang.hitch(this, function(response, io) {
                // Overhead trace is now complete
                this._runningOverhead = false;
                // if we're not running either trace at this point, we're done
                if (this._runningOverhead == false && this._runningUnderground == false) tc.resolve();
              }), lang.hitch(this, function(error, io) {
                console.log("Error processing results of overhead trace:", error);
                this.widgetStandby.hide();
              }));
            }));

          }));
        }

        if (this._tracetype == "underground" || this._tracetype == "both") {
          this._runningUnderground = true;
          var gp = new Geoprocessor(this.undergroundGPURL);
          if (this.userSite == "PNM") {
            var params = {"Feeder_ID": ""+registry.byId('feederid').value,
                          "Buffer_Distance": registry.byId('buffer').value+" Feet"
                         };
          } else if (this.userSite == "TNMP") {
            var params = {"Expression_for_Starting_Flag__Feeder_Id_": "FEEDERID = '"+registry.byId('feederid').value+"'",
                          "Distance__value_or_field_": { "distance": registry.byId('buffer').value, "units": "esriFeet" }
                         };
          }
          gp.submitJob(params);
          gp.on('job-complete', lang.hitch(this, function(response) {

            if (response.jobInfo.jobStatus == "esriJobFailed") {
              this._showError("Underground trace failed.");
              console.log("Underground trace script returned fail.", response);
              this.widgetStandby.hide();
              return;
            }

            // Sleep for 2 seconds to give files ample time to be written
            var sleep = new Deferred();
            setTimeout(function() { sleep.resolve(); }, 2000);
            sleep.then(lang.hitch(this, function() {
              var uresultsFinished = 0;
              var uresultsDef = new Deferred();

              array.forEach(this.undergroundDataModel, lang.hitch(this, function(thisDataModel) {
                new esriRequest({"url": this.undergroundRESURL + "/" + response.jobInfo.jobId + "/scratch/" + thisDataModel + ".json"})
                    .then(lang.hitch(this,function(response,io) {
                      var projDef = new Deferred();

                      if (response.spatialReference != this.map.map.spatialReference) { // need to project geometry

                        var params = new ProjectParameters();
                        params.geometries = [];
                        params.outSR = this.map.map.spatialReference;

                        array.forEach(response.features, function(f) {
                          params.geometries.push(new Point(f.geometry.x, f.geometry.y, new SpatialReference(response.spatialReference)));
                        });

                        var gsvc = new GeometryService(this.Geometry_Server);
                        gsvc.project(params, lang.hitch(this, function(p) {
                          array.forEach(p, lang.hitch(this, function(f, i) { response.features[i].geometry = f; }));
                          projDef.resolve();
                        }));

                      } else { projDef.resolve(); }

                      projDef.then(lang.hitch(this, function() {
                        if (!this._traceResultFeatures) this._traceResultFeatures = response.features;
                        else this._traceResultFeatures = this._traceResultFeatures.concat(response.features);
                        if (++oresultsFinished == this.overheadDataModel.length) oresultsDef.resolve();
                      }));
                    }),lang.hitch(this,function(error, io) {
                      this._showError("Error retrieving underground trace results.");
                    }));
              }));

              uresultsDef.then(lang.hitch(this, function(response, io) {  // Underground trace is now complete
                this._runningUnderground = false;
                // if we're not running either trace at this point, we're done
                if (this._runningOverhead == false && this._runningUnderground == false) tc.resolve();
              }), lang.hitch(this, function(error, io) {
                this._showError("Error displaying results of underground trace:", error);
              }));
            }));

          }));
        }

        // This resolves when all features have been fetched from above
        tc.then(lang.hitch(this, function() {
          this.widgetStandby.hide();
          var traceResultsPane = registry.byId('traceResultsPane');
          var tabContainer = registry.byId('tabContainer');
          traceResultsPane.set('disabled', false);
          tabContainer.selectChild('traceResultsPane', false);

          document.getElementById('resultsLink').innerHTML = this._traceResultFeatures.length;
          if (this._tracetype == "overhead") { document.getElementById('resultsARLabel').innerHTML = "Add/Remove Above Ground Features:"; }
          else if (this._tracetype == "underground") { document.getElementById('resultsARLabel').innerHTML = "Add/Remove Underground Features:"; }
          else if (this._tracetype == "both") { document.getElementById('resultsARLabel').innerHTML = "Add/Remove All Features:"; }

          this._drawResultsGraphics();

          on(registry.byId('resultsOpenRT'), 'click', lang.hitch(this, this._showResultsTool));
          on(document.getElementById('resultsLink'), 'click', lang.hitch(this, function(e) {
            e.preventDefault();
            this._showResultsTool();
            return false;
          }));

          on(registry.byId("resultsZoomResults"), "click", lang.hitch(this, function() {
            var newExtent = null;
            array.forEach(this._getGraphicsLayer().graphics, lang.hitch(this, function(g) {
              if (g.attributes.TYPE == "RESULT") {
                if (!newExtent) newExtent = lang.clone(g._extent);
                newExtent.xmax = (g._extent.xmax > newExtent.xmax) ? g._extent.xmax : newExtent.xmax;
                newExtent.ymax = (g._extent.ymax > newExtent.ymax) ? g._extent.ymax : newExtent.ymax;
                newExtent.xmin = (g._extent.xmin < newExtent.xmin) ? g._extent.xmin : newExtent.xmin;
                newExtent.ymin = (g._extent.ymin < newExtent.ymin) ? g._extent.ymin : newExtent.ymin;
              }
            }));
            newExtent.xmax = newExtent.xmax+250; newExtent.xmin = newExtent.xmin-250;
            newExtent.ymax = newExtent.ymax+65;  newExtent.ymin = newExtent.ymin-65;
            this.map.map.setExtent(newExtent);
          }));

          on(registry.byId("resultsZoomSource"), "click", lang.hitch(this, function() {
            var newExtent = null;
            array.forEach(this._getGraphicsLayer().graphics, lang.hitch(this, function(g) {
              if (g.attributes.TYPE == "SOURCE") { newExtent = lang.clone(g._extent); }
            }));
            newExtent.xmax = newExtent.xmax+250; newExtent.xmin = newExtent.xmin-250;
            newExtent.ymax = newExtent.ymax+65;  newExtent.ymin = newExtent.ymin-65;
            this.map.map.setExtent(newExtent);
          }));

          on(registry.byId("resultsClear"), "click", lang.hitch(this, function() {
            // this is a hard reset apparently
            this._getGraphicsLayer().clear();
            if (this._resultsToolPanel) {
              this._resultsToolPanel.close();
            }
            this._resultsPanelLayer = null;
            this._traceResultFeatures = null;
            var traceResultsPane = registry.byId('traceResultsPane');
            var tabContainer = registry.byId('tabContainer');
            traceResultsPane.set('disabled', true);
            tabContainer.selectChild('performTracePane', false);
          }));

          on(registry.byId("resultsAdd"), "click", lang.hitch(this, function() {
            var selectTool = new Draw(this.map.map);
            selectTool.activate(Draw.EXTENT);
            on(selectTool, 'draw-complete', lang.hitch(this,function(res) {
              this._getFeaturesInGeo(res.geometry);
              selectTool.deactivate();
            }));
          }));

          on(registry.byId("resultsRemove"), "click", lang.hitch(this, function() {
            var selectTool = new Draw(this.map.map);
            selectTool.activate(Draw.EXTENT);
            on(selectTool, 'draw-complete', lang.hitch(this,function(res) {
              var geometry = res.geometry;
              var tempResultFeatures = [];
              array.forEach(this._traceResultFeatures, lang.hitch(this, function(f, i) {
                if (f.geometry.x >= geometry.xmin && f.geometry.x <= geometry.xmax && f.geometry.y >= geometry.ymin && f.geometry.y <= geometry.ymax) {
                } else {
                  tempResultFeatures.push(f);
                }
              }));
              this._traceResultFeatures = tempResultFeatures;
              document.getElementById('resultsLink').innerHTML = this._traceResultFeatures.length;
              this._drawResultsGraphics();
              selectTool.deactivate();
            }));
          }));

        }));
      }));

    },

    _showResultsTool: function() {
      if (!this._resultsToolPanel) {
        this._resultsPanelLayer = new GraphicsLayer({id: "GJTraceToolResultsPanelLayer"});
        this.map.map.addLayer(this._resultsPanelLayer);

        dojo.require("ibm.tivoli.fwm.mxmap.toolbar.ext.GJTraceToolResultsPanelWidget");
        this._resultsToolPanel = new GJTraceToolResultsPanelWidget({
          id: 'GJTraceToolResultsPanel',
          map: this.map,
          tool: this,
          dialogPosition: this.dialogPosition,
          resultsGraphicLayer: this._resultsPanelLayer,
          resultSpatialReference: this.resultSpatialReference
        });

        this._resultsToolPanel.createDialog();
      }
      if (!this._resultsToolPanel.infoPanel._theDialog.open) { this._resultsToolPanel.showDialog(); }
      this._resultsToolPanel.addFeaturesToResults(this._traceResultFeatures);
    },

    enableHighlightButton: function() { }, // do nothing, just a necessity override
    disableHighlightButton: function() { }, // same
    enableActions: function() { }, // same
    disableActions: function() { }, // same

    _populateSubstations: function() {
      this.loadingSubstationsStandby.show();
      var queryTask = new QueryTask(this.Substation_Layer);
      var query = new esriQuery();
      query.where = "SUBSTATIONNAME IS NOT null";
      if (this.userSite == "PNM") query.outFields = ['OBJECTID', 'SUBSTATIONNAME'];
      else if (this.userSite == "TNMP") query.outFields = ['SUBSTATIONNAME'];
      query.orderByFields = ['SUBSTATIONNAME'];
      query.returnDistinctValues = true;
      query.returnGeometry = false;
      queryTask.execute(query, lang.hitch(this, function(result) {
        var namesDef = new Deferred();
        var substationNames = array.map(result.features, lang.hitch(this, function(feature, i) {
          if(feature.attributes.SUBSTATIONNAME) {
            if((i + 1) === result.features.length) namesDef.resolve();
            if (this.userSite == "PNM") return {name: feature.attributes.SUBSTATIONNAME, id: feature.attributes.OBJECTID};
            else if (this.userSite == "TNMP") return {name: feature.attributes.SUBSTATIONNAME, id: feature.attributes.SUBSTATIONNAME};
          }
        }));
        namesDef.then(lang.hitch(this, function() {
          if (substationNames.length > 0) {
            var store = new Memory({ data: substationNames });
            this.populateWithStore("substation", store);
            this.loadingSubstationsStandby.hide();
          }
        }, lang.hitch(this, function(err){
          this._showError("There was an error displaying substation names. ERROR: " + err);
        })));
      }), lang.hitch(this, function(err){
        this._showError("There was an error querying substation names. ERROR: " + err);
      }));
    },

    _populateFeeders: function(substationID) {
      this.loadingFeedersStandby.show();
      var namesDef = new Deferred();
      this._feederNames = [];

      if (this.userSite == "PNM") {
        var queryTask = new QueryTask(this.Feeder_Layer);
        var query = new esriQuery();
        query.where = "ELECTRICSTATIONOBJECTID='"+substationID+"' AND SUBTYPECD='"+this.Feeder_Subtype+"'";
        query.outFields = ['FEEDERID'];
        query.orderByFields = ['FEEDERID'];
        query.outSpatialReference = this.map.map.spatialReference;
        query.returnGeometry = true;
        queryTask.execute(query, lang.hitch(this, function(result) {
          this._feedersGeo = {};
          array.forEach(result.features, lang.hitch(this, function(f) {
            this._feedersGeo[f.attributes.FEEDERID] = f.geometry;
          }));
          this._feederNames = array.map(result.features, lang.hitch(this, function(feature, i) {
            if(feature.attributes.FEEDERID) {
              return { name: feature.attributes.FEEDERID, id: feature.attributes.FEEDERID };
            }
          }));
          namesDef.resolve();
        }), lang.hitch(this, function(err){
          this._showError("There was an error querying feeders. ERROR: " + err);
        }));
      } else if (this.userSite == "TNMP") {
        var queryTask = new QueryTask(this.Substation_Layer);
        var query = new esriQuery();
        query.where = "SUBSTATIONNAME='"+substationID+"'";
        query.outFields = ['MISCNETWORKFEATUREOBJECTID'];
        query.returnGeometry=false;
        query.returnDistinctValues=true;
        queryTask.execute(query, lang.hitch(this, function(result) {
          var feederOBJECTIDs = array.map(result.features, lang.hitch(this, function(feature, i) {
            if (feature.attributes.MISCNETWORKFEATUREOBJECTID) {
              return feature.attributes.MISCNETWORKFEATUREOBJECTID;
            }
          }));
          var queryTaskF = new QueryTask(this.Feeder_Layer);
          var queryF = new esriQuery();
          queryF.objectIds = feederOBJECTIDs;
          queryF.outFields = ['FEEDERID'];
          queryF.where = "SUBTYPECD='"+this.Feeder_Subtype+"'";
          queryF.returnGeometry=true;
          queryF.outSpatialReference = this.map.map.spatialReference;
          queryF.orderByFields = ['FEEDERID'];
          queryTaskF.execute(queryF, lang.hitch(this, function(resultf) {
            this._feedersGeo = {};
            array.forEach(resultf.features, lang.hitch(this, function(f) {
              this._feedersGeo[f.attributes.FEEDERID] = f.geometry;
            }));
            this._feederNames = array.map(resultf.features, lang.hitch(this, function(feature, i) {
              if (feature.attributes.FEEDERID) {
                return { name: feature.attributes.FEEDERID, id: feature.attributes.FEEDERID };
              }
            }));
            namesDef.resolve();
          }), lang.hitch(this, function(err) {
            this._showError("There was an error querying feeders. ERROR: " + err);
          }));
        }), lang.hitch(this, function(err) {
          this._showError("There was an error querying feeders. ERROR: " + err);
        }));
      }

      namesDef.then(lang.hitch(this, function() {
        if (this._feederNames.length > 0) {
          var store = new Memory({ data: this._feederNames });
          this.populateWithStore("feederid", store);
          registry.byId("feederid").setAttribute('disabled', false);
          registry.byId("feederid").set("item", store.get(this._feederNames[0].id));
          this.loadingFeedersStandby.hide();
        }
      }, lang.hitch(this, function(err){
        this._showError("There was an error displaying feeders. ERROR: " + err);
      })));

    },

    populateWithStore: function(dijitID, store) {
      var node = registry.byId(dijitID);
      var emptyStore = new Memory({data:[]});
      node.setAttribute("store", emptyStore);
      node.setAttribute("store", store);
    },

    _getFeaturesInGeo: function(geo) {
      var tc = new Deferred();
      this.addStandby.show();
      if (this._tracetype == "overhead" || this._tracetype == "both") {
        this._runningOverhead = true;
        var oresultsFinished = 0;
        var oresultsDef = new Deferred();

        array.forEach(this.Overhead_Feature_Layers, lang.hitch(this, function(u) {
          var qt = new QueryTask(u.URL);
          var query = new esriQuery();
          var subtypeQ = [];
          array.forEach(u.Subtypes, function(subtype) { subtypeQ.push("SUBTYPECD='"+subtype+"'"); });
          query.where = subtypeQ.join(" OR ");
          query.geometryType = 'esriGeometryPolygon';
          query.geometry = geo;
          query.outFields = ['OBJECTID', 'MXASSETNUM'];
          query.outSpatialReference = this.map.map.spatialReference;
          query.returnGeometry = true;
          qt.execute(query, lang.hitch(this, function(response) {
            if (!this._traceResultFeatures) this._traceResultFeatures = response.features;
            else this._traceResultFeatures = this._traceResultFeatures.concat(response.features);
            if (++oresultsFinished == this.Overhead_Feature_Layers.length) oresultsDef.resolve();
          }), lang.hitch(this, function(error, io) {
            this._showError("Error adding features to results - overhead", error);
            this.widgetStandby.hide();
          }));
        }));

        oresultsDef.then(lang.hitch(this, function(response, io) {
          this._runningOverhead = false;
          if (this._runningOverhead == false && this._runningUnderground == false) tc.resolve();
        }), lang.hitch(this, function(error, io) {
          this._showError("Error displaying results of overhead trace:", error);
        }));

      }

      if (this._tracetype == "underground" || this._tracetype == "both") {
        this._runningUnderground = true;
        var uresultsFinished = 0;
        var uresultsDef = new Deferred();

        array.forEach(this.Underground_Feature_Layers, lang.hitch(this, function(u) {
          var qt = new QueryTask(u.URL);
          var query = new esriQuery();
          var subtypeQ = [];
          array.forEach(u.Subtypes, function(subtype) { subtypeQ.push("SUBTYPECD='"+subtype+"'"); });
          query.where = subtypeQ.join(" OR ");
          query.geometryType = 'esriGeometryPolygon';
          query.geometry = geo;
          query.outSR = this.map.map.spatialReference;
          query.outFields = ['OBJECTID', 'MXASSETNUM'];
          query.returnGeometry = true;
          qt.execute(query, lang.hitch(this, function(response) {
            if (!this._traceResultFeatures) this._traceResultFeatures = response.features;
            else this._traceResultFeatures = this._traceResultFeatures.concat(response.features);
            if (++uresultsFinished == this.Underground_Feature_Layers.length) uresultsDef.resolve();
          }), lang.hitch(this, function(error, io) {
            this._showError("Error adding features to results - underground", error);
            this.widgetStandby.hide();
          }));
        }));

        uresultsDef.then(lang.hitch(this, function(response, io) {
          this._runningUnderground = false;
          if (this._runningOverhead == false && this._runningUnderground == false) tc.resolve();
        }), lang.hitch(this, function(error, io) {
          this._showError("Error displaying results of underground add:", error);
        }));
      }

      tc.then(lang.hitch(this, function() {
        var uniqueResults = [];
        array.forEach(this._traceResultFeatures, lang.hitch(this, function(f) {
          var found = false;
          array.forEach(uniqueResults, function(uR) {
            if (!f.geometry.type || f.geometry.type == "point") {
              if (uR.geometry.x == f.geometry.x && uR.geometry.y == f.geometry.y) found = true;
            } else if (f.geometry.type == "polygon") {
              if (uR.geometry.rings == f.geometry.rings) found = true;
            }
          });
          if (!found) uniqueResults.push(f);
        }));
        this._traceResultFeatures = uniqueResults;
        document.getElementById('resultsLink').innerHTML = this._traceResultFeatures.length;
        this._drawResultsGraphics();
        this.addStandby.hide();
      }));
    },

    _getGraphicsLayer: function() {
      if (!this._graphicsLayer) {
       this._graphicsLayer = new GraphicsLayer({id:"traceToolLayer"});
       this.map.map.addLayer(this._graphicsLayer);
      }
      return this._graphicsLayer;
    },

    _drawResultsGraphics: function() {
      this._getGraphicsLayer().clear();
      // add marker for "Source" of trace
      var feederID = registry.byId('feederid').value;
      var feederGeo = this._feedersGeo[feederID];
      extCenterPoint = new Point(feederGeo.x, feederGeo.y, feederGeo.spatialReference);
      var marker = new SimpleMarkerSymbol(
        SimpleMarkerSymbol.STYLE_DIAMOND, // style
        28, // size
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,150,0]), 1), // outline
        new Color([0,200,0,0.5]) // fill color
      );
      var attr = {"TYPE": "SOURCE", "FEEDERID":feederID};
      var infoTemplate = new InfoTemplate("Source of Trace","Feeder ID: ${FEEDERID}");
      this._getGraphicsLayer().add(new Graphic(extCenterPoint, marker, attr, infoTemplate));

      array.forEach(this._traceResultFeatures, lang.hitch(this, function(f) {
        if (!f.geometry.type || f.geometry.type == "point") {
          var thisPoint = new Point(f.geometry.x, f.geometry.y, this.map.map.spatialReference);
          var marker = new SimpleMarkerSymbol(
            SimpleMarkerSymbol.STYLE_CIRCLE, // style
            24, // size
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255,0,0]), .25), // outline
            new Color([255,255,0,0.25]) // fill color
          );
        } else if (f.geometry.type == "polygon") {
          var thisPoint = new Polygon(f.geometry);
          var marker = new SimpleMarkerSymbol(
            SimpleMarkerSymbol.STYLE_SQUARE,
            16,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,255]), 1),
            new Color([0,0,255,0.25])
          );
        }
        var attr = {"TYPE": "RESULT"};
        this._getGraphicsLayer().add(new Graphic(thisPoint, marker, attr));
      }));
    },

    _showError: function(message) {
      alert(message);
    },

    showPanel: function () {
      if (this.infoPanel) this.infoPanel.show();
    },

    closePanel : function () {
      if (this.infoPanel) this.infoPanel.hide();
    },

  } );
  return "GJTraceToolPanelWidget";
} );
